#include "Controller.h"

IController *IController::m_pActiveController = nullptr;

IController::~IController()
{
	for (IWorker *pWorker : m_workers)
	{
		pWorker->destroy_worker();
	}

	m_workers.clear();

	if (this == m_pActiveController)
	{
		m_pActiveController->m_iActiveWorker = -1;
		m_pActiveController = nullptr;
	}
}

void IController::RegisterWorkerType(IWorker * pNewWorker)
{
	if (!pNewWorker)
		return;

	RegisterWorker_Internal(pNewWorker);
	pNewWorker->on_worker_registered();
}

void IController::UnregisterWorker(IWorker *pWorker)
{
	worker::List<IWorker *> new_workers;
	for (IWorker *worker : m_workers)
	{
		if (worker == pWorker)
		{
			if (pWorker == GetActiveWorker())
				m_iActiveWorker = -1;

			continue;
		}

		new_workers.emplace_back(pWorker);
	}

	m_workers.clear();
	m_workers = new_workers;
}

IWorker * IController::FindWorkerByTypeName(std::string workerTypeName)
{
	if (workerTypeName.empty())
		return nullptr;

	return FindWorkerByTypeName_Internal(workerTypeName);
}

void IController::SetActiveWorker(std::string workerTypeName)
{
	m_iActiveWorker = GetWorkerIndex(workerTypeName);
	if(m_iActiveWorker > -1)
	{
		m_workers[m_iActiveWorker]->on_became_active();
	}
}

void IController::SetActiveWorker(IWorker * pWorker)
{
	if (!pWorker)
		m_iActiveWorker = -1;

	SetActiveWorker(pWorker->get_worker_type());
}

IWorker * IController::GetActiveWorker()
{
	if (m_iActiveWorker < 0)
		return nullptr;

	return m_workers[m_iActiveWorker];
}

void IController::SetActiveController(IController * pController)
{
	m_pActiveController = pController;

	if (m_pActiveController)
		m_pActiveController->OnBecameActive();
}

IController * IController::GetActiveController()
{
	return m_pActiveController;
}

void IController::RegisterWorker_Internal(IWorker * pNewWorker)
{
	m_workers.emplace_back(pNewWorker);
}

IWorker * IController::FindWorkerByTypeName_Internal(std::string workerTypeName)
{
	for (IWorker * pWorker : m_workers)
	{
		if (pWorker->get_worker_type() == workerTypeName)
			return pWorker;
	}

	return nullptr;
}

int IController::GetWorkerIndex(std::string workerTypeName)
{
	for (int i = 0; i < m_workers.size(); i++)
	{
		IWorker *pWorker = m_workers[i];
		if (pWorker->get_worker_type() == workerTypeName)
			return i;
	}

	return -1;
}

int IController::GetWorkerIndex(IWorker * pWorker)
{
	if (!pWorker)
		return -1;

	return GetWorkerIndex(pWorker->get_worker_type());
}
