// Copyright (c) Improbable Worlds Ltd, All Rights Reserved

#pragma once

#include "Paths.h"
#include "PlatformProcess.h"

/**
* This class ensures that the CoreSdkDll is loaded before it is needed by code.
*/
class FSpatialOSSdkLoader
{
public:
	FSpatialOSSdkLoader()
	{
		// Disable FPaths::GameDir deprecation warning until <= 4.17 is unsupported.
		PRAGMA_DISABLE_DEPRECATION_WARNINGS
			FString Path = FPaths::GameDir() / TEXT("Source/TheCompanions/SpatialOS/worker_sdk");
		PRAGMA_ENABLE_DEPRECATION_WARNINGS

#if PLATFORM_WINDOWS
			Path = Path / TEXT("Windows64Dynamic/CoreSdkDll.dll");
		CoreSdkHandle = FPlatformProcess::GetDllHandle(*Path);
#else
			Path = Path / TEXT("Linux64Dynamic/libCoreSdkDll.so");
		CoreSdkHandle = FPlatformProcess::GetDllHandle(*Path);
#endif
	}

	~FSpatialOSSdkLoader()
	{
		if (CoreSdkHandle != nullptr)
		{
			FPlatformProcess::FreeDllHandle(CoreSdkHandle);
			CoreSdkHandle = nullptr;
		}
	}

	FSpatialOSSdkLoader(const FSpatialOSSdkLoader& rhs) = delete;
	FSpatialOSSdkLoader& operator=(const FSpatialOSSdkLoader& rhs) = delete;

private:
	void* CoreSdkHandle = nullptr;
};
