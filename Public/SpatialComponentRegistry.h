#pragma once
#include "CoreMinimal.h"
#include "SpatialOS/ComponentRegistry.h"
#include "SpatialComponentRegistry.generated.h"

/* Base component registry class to be derived from. It can't be derived from in blueprint, because it has to have valid components registration. Every registry should derive from it as well as derive from IComponentRegistry. */
UCLASS(BlueprintType)
class USpatialComponentRegistry
	: public UObject
{
	GENERATED_BODY()
};