#pragma once
#include "EngineMinimal.h"
#include "EntityComponent.h"
#include "Utilities.h"
#include "standard_library.h"
#include "SpatialWorker.h"
#include "SpatialEntityComponent.generated.h"

UCLASS()
class THECOMPANIONS_API USpatialBaseComponent
	: public UActorComponent
{
	GENERATED_BODY()

public:
	/* Override this method in order to receive component initialization from the entity. */
	virtual void RegisterSpatialComponent(const worker::EntityId initial_id, const worker::Authority initial_authority, const worker::Entity & entity) {}

	/* Override this method in order to receive component initialization. internally it should call on_component_snapshot method to get the right info from the game. */
	virtual void RegisterSpatialComponentFromInternalSnapshot(const worker::EntityId initial_id, const worker::Authority initial_authority) {}

	virtual worker::ComponentId GetComponentId() { return 0; }

	virtual worker::Authority GetOfflineAuthority() { return worker::Authority::kNotAuthoritative; }

	virtual void ApplyToEntity(worker::Entity & entity) {}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SpatialOS Component")
	EComponentAuthority OfflineAuthority = EComponentAuthority::eSCA_NonAuthoritative;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpatialOS Component")
	bool EnableBackwardUpdates = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpatialOS Component")
	bool BlockSendingUpdates = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpatialOS Component")
	bool BlockReceivingUpdates = false;
};

UCLASS(BlueprintType)
class THECOMPANIONS_API USpatialEntityComponentDataForge
	: public USpatialComponentDataForge
{
	GENERATED_BODY()

public:
	virtual void ApplyToEntity(worker::Entity & entity) override;

	UFUNCTION(BlueprintPure, Category = "SpatialOS Component Data Forge")
	static USpatialComponentDataForge *Create(FWorkerRequirementSet write_access, FVector coords);

	improbable::PositionData Get();
	//
	UPROPERTY(BlueprintReadOnly, Category = "SpatialOS Component Data Forge")
	FVector Coords;
	//
};

/** This component is required for each actor that is required to exist in spatial networking system. It sends position rarely, this position is only used for inspector updates and is not used in the game world. No one should get updates from this component. It only sends them. */
UCLASS(BlueprintType, Blueprintable, meta = (BlueprintSpawnableComponent))
class THECOMPANIONS_API USpatialEntityComponent
	: public USpatialBaseComponent
	, public IComponent<improbable::Position>
{
	GENERATED_BODY()

public:
	/* GENERATED */	
	UFUNCTION(BlueprintCallable, Category = "SpatialOS Component")
	static void RegisterDispatcher();
	virtual void RegisterSpatialComponent(const worker::EntityId initial_id, const worker::Authority initial_authority, const worker::Entity & input_entity) override;
	virtual void RegisterSpatialComponentFromInternalSnapshot(const worker::EntityId initial_id, const worker::Authority initial_authority) override;
	virtual worker::ComponentId GetComponentId() override;
	virtual improbable::PositionData snapshot_component() override;
	UFUNCTION(BlueprintNativeEvent, Category = "SpatialOS Component", meta = (DisplayName = "OnSnapshotComponent"))
	void OnSnapshotComponentEvent(FVector & Coords);
	virtual void OnSnapshotComponentEvent_Implementation(FVector & Coords);
	virtual worker::Authority GetOfflineAuthority() override;
	virtual void ApplyToEntity(worker::Entity & entity) override;
	virtual worker::Authority get_offline_authority() override;
	virtual bool are_backward_updates_enabled() override;
	virtual bool is_sending_updates_blocked() override;
	virtual bool is_receiving_updates_blocked() override;
	/* ~GENERATED */

	/* CUSTOM */
	USpatialEntityComponent();
	virtual void on_authority_changed() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction) override;
	bool IsOfflineEnabled();
	bool IsPersistent();
	UFUNCTION(BlueprintPure, Category = "SpatialOS Component")
	FWorkerRequirementSet GetReadAccessRequirements();
	UFUNCTION(BlueprintPure, Category = "SpatialOS Component")
	FWorkerRequirementSet GetAuthorityGrantingWorkerRequirements();
	/* ~CUSTOM */

protected:
	/** Defines how often position is sent to the network. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SpatialOS Entity")
	float InspectorPositionUpdateRate = 5.f;

	/** Defines whether entity is persistent. It means that it is saved regularly by the server into snapshots that allow to re-run server with saved entities and their states. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SpatialOS Entity")
	bool Persistent = true;

	/** Defines whether owner of this component can take a part in offline game. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SpatialOS Entity")
	bool OfflineEnabled = true;

	/** Defines what workers can read from this entity. This setting is per entity, not per component. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SpatialOS Entity")
	TArray<TSubclassOf<USpatialWorker>> ReadAccessWorkers;

	/** Defines which worker will be able to manage and grant authority over this entity. It should not be client for release. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SpatialOS Entity")
	TSubclassOf<USpatialWorker> AuthorityGrantingWorker;
};
