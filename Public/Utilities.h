#pragma once
#include "EngineMinimal.h"
#include "improbable/worker.h"
#include "improbable/standard_library.h"
#include "Utilities.generated.h"

USTRUCT(BlueprintType)
struct THECOMPANIONS_API FSpatialEntityId
{
	GENERATED_USTRUCT_BODY()

public:
	FSpatialEntityId();

	FSpatialEntityId(const FSpatialEntityId& entityId);

	FSpatialEntityId(const worker::EntityId& entityId);

	FSpatialEntityId& operator=(const FSpatialEntityId& other);

	bool operator==(const FSpatialEntityId& other) const;

	bool operator==(const worker::EntityId& other) const;

	bool operator!=(const FSpatialEntityId& other) const;

	worker::EntityId ToSpatial();

private:
	worker::EntityId m_entityId;

	friend uint32 GetTypeHash(FSpatialEntityId const& Rhs);
};

/** Component authority enumeration. */
UENUM(BlueprintType)
enum class EComponentAuthority : uint8
{
	eSCA_NonAuthoritative UMETA(DisplayName = "NotAuthoritative"),
	eSCA_Authoritative UMETA(DisplayName = "Authoritative"),
	eSCA_AuthorityLossImminent UMETA(DisplayName = "AuthorityLossImminent"),
};

USTRUCT(BlueprintType)
struct FWorkerAttributeSet
{
	GENERATED_BODY()

		UPROPERTY(BlueprintReadWrite)
		FString Attribute;

	improbable::WorkerAttributeSet ToSpatial();

	void Update(improbable::WorkerAttributeSet Value);
};

USTRUCT(BlueprintType)
struct FWorkerRequirementSet
{
	GENERATED_BODY()

		UPROPERTY(BlueprintReadWrite)
		TArray<FWorkerAttributeSet> AttributeSet;

	improbable::WorkerRequirementSet ToSpatial();

	void Update(improbable::WorkerRequirementSet Value);

	/** Handy function if we need to have only requirement set with one attribute. */
	void UpdateForSingleAttribute(FString AttributeValue)
	{
		FWorkerAttributeSet Attribute;
		Attribute.Attribute = AttributeValue;
		AttributeSet.Add(Attribute);
	}
};

UCLASS(BlueprintType)
class THECOMPANIONS_API USpatialComponentDataForge
	: public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, Category = "SpatialOS Component Data Forge")
	FWorkerRequirementSet WriteAccess;

	virtual void ApplyToEntity(worker::Entity & entity) {}
};

USTRUCT(BlueprintType)
struct FEntitySpawnInfo
{
	GENERATED_BODY()

	void DestroySpawnInfo();

	void ApplyToEntity(worker::Entity & entity);

	/** Class of the actor to be spawned for this entity. */
	UPROPERTY(BlueprintReadWrite, Category = "SpatialOS Entity Spawn Info")
	TSubclassOf<AActor> ActorClass;
	/** Timeout - in milliseconds. If exceeded, spawn will fail. */
	UPROPERTY(BlueprintReadWrite, Category = "SpatialOS Entity Spawn Info")
	int Timeout = 10000;
	/** Gathers all components that should be spawned with this actor and their initial data. User should not forget any component that this actor contains. Otherwise actor will not have this component in the network, and any updates will fail, and game might crash as some pointers might stay uninitialized. */
	UPROPERTY(BlueprintReadWrite, Category = "SpatialOS Entity Spawn Info")
	TArray<USpatialComponentDataForge*> Forges;
	/** Defines what workers can read from this entity. */
	UPROPERTY(BlueprintReadWrite, Category = "SpatialOS Entity Spawn Info")
	TArray<FWorkerRequirementSet> ReadAccess;
	/** If entity id is not provided, then worker will attempt to reserve one and when reservation returns, it will eventually spawn this entity using that reserved id. */
	UPROPERTY(BlueprintReadWrite, Category = "SpatialOS Entity Spawn Info")
	FSpatialEntityId EntityId;
};

