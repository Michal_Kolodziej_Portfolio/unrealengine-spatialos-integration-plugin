#pragma once
#include "CoreMinimal.h"
#include "Controller.h"
#include "SpatialWorker.h"
#include "SpatialController.generated.h"

UCLASS(BlueprintType, Blueprintable)
class THECOMPANIONS_API USpatialController
	: public UObject
	, public IController
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category = "SpatialOS Networking")
	static void ActivateSpatialController(TSubclassOf<USpatialController> ControllerClass);

	UFUNCTION(BlueprintCallable, Category = "SpatialOS Networking")
	static void SetActiveSpatialController(USpatialController *Controller);

	UFUNCTION(BlueprintPure, Category = "SpatialOS Networking")
	static USpatialController *GetActiveSpatialController();

	UFUNCTION(BlueprintCallable, Category = "SpatialOS Networking")
	USpatialWorker *GetWorkerByClass(TSubclassOf<USpatialWorker> WorkerClass);

	UFUNCTION(BlueprintCallable, Category = "SpatialOS Networking")
	void ActivateSpatialWorker(const FString WorkerTypeName);

	UFUNCTION(BlueprintCallable, Category = "SpatialOS Networking")
	void ActivateSpatialWorkerByClass(TSubclassOf<USpatialWorker> WorkerClass);

	UFUNCTION(BlueprintCallable, Category = "SpatialOS Networking")
	void RegisterWorker(TSubclassOf<USpatialWorker> WorkerClass);

	UFUNCTION(BlueprintCallable, Category = "SpatialOS Networking")
	void UnregisterSpatialWorker(USpatialWorker *Worker);

	UFUNCTION(BlueprintPure, Category = "SpatialOS Networking")
	USpatialWorker *GetWorkerByTypeName(FString WorkerTypeName);

	UFUNCTION(BlueprintPure, Category = "SpatialOS Networking")
	USpatialWorker *GetActiveSpatialWorker();

	/** User should register workers at this point. It is the best place to do this, as after this event is called, controller automatically activates worker if specified. */
	UFUNCTION(BlueprintNativeEvent, Category = "SpatialOS Networking", meta = (DisplayName = "OnBecomeActive"))
	void OnBecomeActiveEvent();
	virtual void OnBecomeActiveEvent_Implementation() {}

	virtual void OnBecameActive() override;

protected:
	/** Classes of the workers that will be registered by this controller. Those classes will be registered when this controller becomes active, before OnBecomeActive event is called, but only if AutoRegisterWorkers is true. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpatialOS Networking")
	TArray<TSubclassOf<USpatialWorker>> AutoRegisterWorkerClasses;

	/** This default worker class will be used if user does not provide worker name by using spatialos_worker_type input parameter. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpatialOS Networking")
	TSubclassOf<USpatialWorker> DefaultWorkerClass;

	/** If this flag is true, controller will automatically register workers specified in RegisteredWorkerClasses when this controller becomes active, but before OnBecomeActive event is called. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpatialOS Networking")
	bool AutoRegisterWorkers = false;

	/** If this flag is true, controller will call activate on worker after becoming active himself. The worker selected is by the default class, but can be overriden by input parameter to the engine. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpatialOS Networking")
	bool ActivateWorkerOnActivation = false;

	/** Unreal UObjects require to be registered as UPROPERTY, but we can't do it for registered workers, because their array sits inside of the controller base class. */
	/** If we leave it this way, the garbage collector will remove workers after some time and there is nothing we can do about it. */
	/** To solve this issue, we will declare here array of registered workers marked as UPROPERTY, and add them to this array too, so that the references are stored. */
	UPROPERTY()
	TArray<USpatialWorker*> m_spatialWorkers;
};