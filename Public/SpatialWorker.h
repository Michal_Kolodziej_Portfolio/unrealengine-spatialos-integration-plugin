#pragma once
#include "Worker.h"
//
#include "Runtime/Engine/Public/Tickable.h"
#include "BlueprintRegistry.h"
#include "Utilities.h"
#include "SpatialComponentRegistry.h"
#include "SpatialOSSdkLoader.h"
#include "SpatialWorker.generated.h"

UENUM(BlueprintType)
enum class ESpatialWorkerConnectionType : uint8
{
	eSWCT_TCP UMETA(DisplayName = "TCP"),
	eSWCT_RAKNET UMETA(DisplayName = "RakNet"),
	eSWCT_KCP UMETA(DisplayName = "KCP"),
};

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnEntityIdsReservedDelegate, TArray<FSpatialEntityId>, ReservedEntityIds);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnEntityQueriedDelegate, TArray<FSpatialEntityId>, QueriedEntityIds);

/* Default spatial worker cannot be derived from in blueprint, because it has to register components, and that can only be done inside C++. It can be used in blueprint though. */
UCLASS(BlueprintType)
class THECOMPANIONS_API USpatialWorker
	: public UObject
	, public FTickableGameObject
	, public SWorker<AActor>
{
	GENERATED_BODY()
public:
	/* FTickableGameObject */
	void Tick(float DeltaTime) override;
	bool IsTickable() const override;
	bool IsTickableInEditor() const override;
	bool IsTickableWhenPaused() const override;
	TStatId GetStatId() const override;
	UWorld* GetWorld() const override;
	/* ~FTickableGameObject */

	USpatialWorker() {}
	~USpatialWorker() {}

	/** Worker should be initialized at the beginning of its existence. Preferably inside its own blueprint script at contruct script of on begin play. WorkerId is empty by default. It will attempt to find value for it provided by input arguments into unreal engine, and if not found, it will generate unique id. */
	UFUNCTION(BlueprintCallable)
	void InitializeSpatialWorker(FString WorkerId = "");

	/** Generates snapshot from currently ran level. */
	UFUNCTION(BlueprintCallable)
	virtual void SnapshotCurrentLevel(const FString SnapshotOutputPath);

	/** Connects to SpatialOS deployment using values passed into this function. */
	UFUNCTION(BlueprintCallable)
	void ConnectToDeployment(const ESpatialWorkerConnectionType ConnectionType, const bool UseExternalIp, const FString IpAddress = "127.0.0.1", const int Port = 7777, const int ConnectionTimeout = 10000, const int HeartbeatTimeout = 60000, const FString ProjectName = "", const FString DeploymentName = "", const FString PlayerToken = "");

	UFUNCTION(BlueprintPure, Category = "SpatialOS Networking")
	FString CreatePlayerIdentityToken(const FString LoginToken, const FString PlayerId, const FString DisplayName);

	/** Connects to SpatialOS deployment using values from input arguments into the engine. */
	UFUNCTION(BlueprintCallable)
	void Connect();

	/** Reserves entity ids within spatialos networking system. */
	UFUNCTION(BlueprintCallable)
	void ReserveEntityIds(int NumberOfIds, const FOnEntityIdsReservedDelegate & CallbackFunction, int Timeout = 5000);

	/** Queries entities in the world based on possessed component. */
	UFUNCTION(BlueprintCallable)
	void QueryEntities(int32 ComponentId, FOnEntityQueriedDelegate CallbackFunction, int Timeout = 5000);

	/** Attempts to create entity in SpatialOS world. If entity id is not provided, or is invalid, it will automatically reserve id for it, and then create entity. */
	UFUNCTION(BlueprintCallable, Category = "SpatialOS Networking")
	void CreateEntity(FEntitySpawnInfo SpawnInfo);

	/** Removes entity based on its id. */
	UFUNCTION(BlueprintCallable)
	void RemoveEntity(FSpatialEntityId EntityId);

	/** Deletes all actors that have spatial entity component. Note that this should be called preferably before Connect. */
	UFUNCTION(BlueprintCallable)
	void DeleteSpatialActors();

	/** Returns actor associated with given EntityId. */
	UFUNCTION(BlueprintPure)
	AActor *GetActorFromDatabase(FSpatialEntityId EntityId);

	/* Used to check if connection is maintained. Returns current connection state. m_pConnection pointer must be valid at the point of checking connection! */
	UFUNCTION(BlueprintPure)
	bool IsConnected();

	/* Returns isOffline flag state. */
	UFUNCTION(BlueprintPure)
	bool IsOffline();

	/* Returns worker unique id. */
	UFUNCTION(BlueprintPure)
	FString GetWorkerId();	

	/* Returns worker type. */
	UFUNCTION(BlueprintPure)
	FString GetWorkerType();

	UFUNCTION(BlueprintPure)
	FWorkerAttributeSet GetWorkerLayer();

	UFUNCTION(BlueprintPure)
	FWorkerRequirementSet GetWorkerRequirementSet();

	/* Returns true if we are in editor. */
	UFUNCTION(BlueprintPure, Category = "SpatialOS Networking")
	bool IsInEditor();

	virtual std::string get_worker_type() override;
	virtual std::string get_worker_layer() override;

	/* Convets unreal actor to spatial entity. */
	bool ConvertActorToEntity(AActor *pActor, worker::Entity & entity, const bool ExcludeNonPersistent);

protected:
	/* PreInitializeNetworking MUST be overriden in the implementation class and return valid view instance and component registry with newly registered components. */
	virtual void pre_initialize_networking(IComponentRegistry *& componentRegistry, worker::View *& view) override {}

	/* Should return valid list of current spatial entities in the game world. It should be user defined and user needs to make ids consistent. */
	virtual worker::List<worker::Entity> get_offline_entities(worker::EntityId begin_id, const bool initialize_components = true) override;

	/* Generates unique guid for worker id. Strength is only if needed, but for UE implementation it isn't needed, so it doesn't effect anything. */
	virtual std::string get_unique_guid(const int strength = 32) override;

	/* Called automatically during Connect method, if connection succeeds. */
	virtual void on_connection_succeeded() override;

	/* Called automatically during Connect method, if connection fails. */
	virtual void on_connection_failed() override;

	/* Called automatically inside OnReserveEntityIds, but it's more handy to use. */
	virtual void on_entity_ids_reserved(const worker::List<worker::EntityId> & reservedIds, const worker::RequestId<worker::ReserveEntityIdsRequest> & reservation) override;

	/* Called automatically inside OnEntityQuery id query succeeds. */
	virtual void on_entity_queried(const worker::Map<worker::EntityId, worker::Entity> & queriedEntities, const worker::RequestId<worker::EntityQueryRequest> & query) override;

	/* Called automatically inside OnAddMetadataComponent, but it's a little bit more high level, as it simply passes entity id and string name of the created entity which is handy. */
	virtual AActor * on_create_entity(const worker::EntityId & entityId, const std::string & entityName) override;

protected:
	UFUNCTION(BlueprintNativeEvent, Category = "SpatialOS Networking", meta=(DisplayName = "OnConnectionSucceeded"))
	void OnConnectionSucceededEvent();
	virtual void OnConnectionSucceededEvent_Implementation() {}

	UFUNCTION(BlueprintNativeEvent, Category = "SpatialOS Networking", meta = (DisplayName = "OnConnectionFailed"))
	void OnConnectionFailedEvent();
	virtual void OnConnectionFailedEvent_Implementation() {}

	UFUNCTION(BlueprintNativeEvent, Category = "SpatialOS Networking", meta = (DisplayName = "OnCreateEntity"))
	AActor *OnCreateEntityEvent(FSpatialEntityId EntityId, const FString & EntityName);
	virtual AActor *OnCreateEntityEvent_Implementation(FSpatialEntityId EntityId, const FString & EntityName) { return nullptr; }

	UFUNCTION(BlueprintNativeEvent, Category = "SpatialOS Networking", meta = (DisplayName = "OnBecomeActive"))
	void OnBecomeActiveEvent();
	virtual void OnBecomeActiveEvent_Implementation() {}

	UFUNCTION(BlueprintNativeEvent, Category = "SpatialOS Networking", meta = (DisplayName = "GetWorkerLayer"))
	FWorkerAttributeSet GetWorkerLayerEvent();
	virtual FWorkerAttributeSet GetWorkerLayerEvent_Implementation() { return FWorkerAttributeSet(); }

	UFUNCTION(BlueprintNativeEvent, Category = "SpatialOS Networking", meta = (DisplayName = "OnWorkerRegistered"))
	void OnWorkerRegistered();
	virtual void OnWorkerRegistered_Implementation() {}
	virtual void on_worker_registered() override;
	virtual void on_became_active() override;

	/** User should register components there. */
	virtual void OnRegisterComponentsDispatchers() {}

	/* Fills entity components and their data based on creation info. */
	//void InitializeEntityFromCreationInfo(FEntityCreationInfo &CreationInfo);
	/* Returns entity registry. */

	IComponentRegistry *GetComponentRegistry();

protected:
	worker::Map<worker::RequestId<worker::ReserveEntityIdsRequest>, FOnEntityIdsReservedDelegate> m_entityIdReservations;
	worker::Map<worker::RequestId<worker::EntityQueryRequest>, FOnEntityQueriedDelegate> m_entityQueries;

	/** Defines spatial component registry class that will be loaded for this worker. Registry basically contains and registers components that spatialos worker can use. They can only be defined in C++. */
	UPROPERTY(EditAnywhere)
	TSubclassOf<USpatialComponentRegistry> ComponentRegistryClass;

	/** Paths to the blueprint folders, that hold actor possible to be spawned by the network. Path should start from "/" Example: /Game/GameTitle/Blueprints/Entities */
	UPROPERTY(EditAnywhere, Category = "SpatialOS Networking")
	TArray<FString> BlueprintEntitiesPaths;

	/** Worker type name is used to identify workers. Worker type name is registered name within the SpatialOS project. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SpatialOS Networking")
	FString WorkerTypeName;

	/** Worker layer name is used to identify which networking layer this worker is meant to simulate. Layers are registered within SpatialOS project. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "SpatialOS Networking")
	FString WorkerLayerName;

	/** Blueprint registry allows for retrieving blueprint classes by their name. */
	UPROPERTY(BlueprintReadOnly, Category = "SpatialOS Networking")
	UBlueprintRegistry *BlueprintRegistry;

private:
	/* This static pointer allows to run spatial modules asynchronously. This is required for worker to run with UE. */
	static FSpatialOSSdkLoader *SpatialSdkLoader;
};