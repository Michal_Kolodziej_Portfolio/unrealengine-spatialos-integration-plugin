#pragma once
#include "CoreMinimal.h"
#include "Engine.h"
#include "BlueprintRegistry.generated.h"

UCLASS(BlueprintType, Blueprintable)
class THECOMPANIONS_API UBlueprintRegistry : public UObject
{
	GENERATED_BODY()

public:
	UBlueprintRegistry()
	{
	}

	UFUNCTION(BlueprintCallable, Category = "BlueprintRegistry")
	void RegisterBlueprints()
	{
		UE_LOG(LogTemp, Warning, TEXT("Blueprints paths to register: %d"), BlueprintLibrariesPaths.Num());
		for (auto& path : BlueprintLibrariesPaths)
		{
			TArray<UObject*> assets;
			if (EngineUtils::FindOrLoadAssetsByPath(path, assets, EngineUtils::ATL_Class))
			{
				UE_LOG(LogTemp, Warning, TEXT("Assets found in blueprints folder %s: %d"), *path, assets.Num());
				for (auto asset : assets)
				{
					UBlueprintGeneratedClass *pBlueprintClass = Cast<UBlueprintGeneratedClass>(asset);
					if (pBlueprintClass)
					{
						FString blueprintName = pBlueprintClass->GetName().LeftChop(2);
						UE_LOG(LogTemp, Display, TEXT("Registering blueprint %s"), *blueprintName);
						RegisterBlueprintClass(blueprintName, pBlueprintClass);
					}
					else
					{
						UE_LOG(LogTemp, Warning, TEXT("Found asset that is not blueprint %s"), *(asset->GetFullName()));
					}
				}
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("No assets found in %s."), *path);
			}
		}
	}
	UFUNCTION(BlueprintCallable, Category = "BlueprintRegistry")
	void RegisterBlueprintClass(const FString className, UClass* classToSpawn)
	{
		if (!classToSpawn)
		{
			UE_LOG(LogTemp, Warning, TEXT("Class to spawn is null"));
			return;
		}
		auto existingClass = ClassMap.Find(className);
		if (existingClass)
		{
			UE_LOG(LogTemp, Warning, TEXT("Class %s is already registered"), *className);
			return;
		}

		ClassMap.Add(className, classToSpawn);
	}
	UFUNCTION(BlueprintCallable, Category = "BlueprintRegistry")
	UClass* GetBlueprintClass(const FString ClassName)
	{
		UClass **classToSpawn = ClassMap.Find(ClassName);
		if (!classToSpawn)
		{
			UE_LOG(LogTemp, Warning, TEXT("No class found for name %s"), *ClassName);
			return nullptr;
		}

		return *classToSpawn;
	}
	UFUNCTION(BlueprintCallable, Category = "BlueprintRegistry")
	FString GetBlueprintClassName(UClass *Class)
	{
		if (!Class)
			return "";

		FString classToReturn = *ClassMap.FindKey(Class);

		return classToReturn;
	}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BlueprintRegistry")
	TArray<FString> BlueprintLibrariesPaths;

private:
	UPROPERTY()
	TMap<FString, UClass*> ClassMap;
};