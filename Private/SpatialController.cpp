#include "Public/SpatialController.h"

void USpatialController::ActivateSpatialController(TSubclassOf<USpatialController> ControllerClass)
{
	if (!IsValid(ControllerClass))
		return;

	SetActiveController(NewObject<USpatialController>((UObject*)GetTransientPackage(), ControllerClass));
}

void USpatialController::SetActiveSpatialController(USpatialController * Controller)
{
	if (!IsValid(Controller))
		return;

	SetActiveController(Controller);
}

USpatialController *USpatialController::GetActiveSpatialController()
{
	return (USpatialController*)m_pActiveController;
}

USpatialWorker * USpatialController::GetWorkerByClass(TSubclassOf<USpatialWorker> WorkerClass)
{
	if (!IsValid(WorkerClass))
		return nullptr;

	UObject *cdo = WorkerClass->ClassDefaultObject;
	USpatialWorker *pCDO = Cast<USpatialWorker>(cdo);

	return GetWorkerByTypeName(pCDO->GetWorkerType());
}

void USpatialController::RegisterWorker(TSubclassOf<USpatialWorker> WorkerClass)
{
	if (!IsValid(WorkerClass))
		return;

	USpatialWorker *pNewWorker = NewObject<USpatialWorker>(this, WorkerClass);
	m_spatialWorkers.Add(pNewWorker);
	RegisterWorkerType(pNewWorker);
}

void USpatialController::UnregisterSpatialWorker(USpatialWorker * Worker)
{
	if (!IsValid(Worker))
		return;

	m_spatialWorkers.Remove(Worker);
	UnregisterWorker(Worker);
}

void USpatialController::ActivateSpatialWorker(const FString WorkerTypeName)
{
	SetActiveWorker(TCHAR_TO_UTF8(*WorkerTypeName));
}

void USpatialController::ActivateSpatialWorkerByClass(TSubclassOf<USpatialWorker> WorkerClass)
{
	if (!IsValid(WorkerClass))
		return;

	for (IWorker *pIWorker : m_workers)
	{
		USpatialWorker * pWorker = static_cast<USpatialWorker*>(pIWorker);
		if (pWorker)
		{
			if (pWorker->IsA(WorkerClass))
			{
				ActivateSpatialWorker(pWorker->GetWorkerType());
				return;
			}
		}
	}
}

USpatialWorker * USpatialController::GetWorkerByTypeName(FString WorkerTypeName)
{
	return static_cast<USpatialWorker*>(FindWorkerByTypeName(TCHAR_TO_UTF8(*WorkerTypeName)));
}

USpatialWorker * USpatialController::GetActiveSpatialWorker()
{
	return static_cast<USpatialWorker*>(GetActiveWorker());
}

void USpatialController::OnBecameActive()
{
	if (AutoRegisterWorkers)
	{
		for (TSubclassOf<USpatialWorker> WorkerClass : AutoRegisterWorkerClasses)
		{
			RegisterWorker(WorkerClass);
		}
	}

	OnBecomeActiveEvent();

	if (ActivateWorkerOnActivation)
	{
		FString worker_type;
		FParse::Value(FCommandLine::Get(), TEXT("spatialos_worker_type"), worker_type);
		if (worker_type.IsEmpty())
		{
			UE_LOG(LogTemp, Warning, TEXT("SpatialOS Controller became active. User has not specified worker type overrider argument, default worker class will be used."));
			if (IsValid(DefaultWorkerClass))
			{
				ActivateSpatialWorkerByClass(DefaultWorkerClass);
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("Currently active SpatialOS Controller does not have default worker class specified. No worker will be marked as active. In order to properly utilize workers, user should activate it manually."));
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Currently active SpatialOS Controller detected overriding parameter of worker type: %s. This worker will be active now."), *worker_type);
			ActivateSpatialWorker(worker_type);
		}
	}
}