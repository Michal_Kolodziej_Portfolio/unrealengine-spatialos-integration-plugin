#include "Public/SpatialWorker.h"
#include "ComponentRegistry.h"
#include "SpatialEntityComponent.h"
#include "CoreMinimal.h"

FSpatialOSSdkLoader *USpatialWorker::SpatialSdkLoader = nullptr;

#pragma region TICKING_LOGIC
void USpatialWorker::Tick(float DeltaTime)
{
	process();
}

bool USpatialWorker::IsTickable() const
{
	return m_pConnection && is_connected();
}

bool USpatialWorker::IsTickableInEditor() const
{
	return false;
}

bool USpatialWorker::IsTickableWhenPaused() const
{
	return false;
}

TStatId USpatialWorker::GetStatId() const
{
	return TStatId();
}

UWorld * USpatialWorker::GetWorld() const
{
	return GWorld->GetWorld();
}
#pragma endregion

void USpatialWorker::InitializeSpatialWorker(FString WorkerId)
{
	/* Create Sdk loader for UE. */
	if(!SpatialSdkLoader) SpatialSdkLoader = new FSpatialOSSdkLoader();

	/* If WorkerId is empty, check if it was provided with input arguments. */
	if (WorkerId.IsEmpty())
	{
		FString worker_id;
		FParse::Value(FCommandLine::Get(), TEXT("spatialos_worker_id"), WorkerId);
	}

	BlueprintRegistry = NewObject<UBlueprintRegistry>();
	BlueprintRegistry->BlueprintLibrariesPaths = BlueprintEntitiesPaths;
	BlueprintRegistry->RegisterBlueprints();

	initialize_worker(TCHAR_TO_UTF8(*WorkerTypeName), TCHAR_TO_UTF8(*WorkerId), TCHAR_TO_UTF8(*WorkerLayerName), GIsEditor);
}

void USpatialWorker::SnapshotCurrentLevel(const FString SnapshotOutputPath)
{
	if (SnapshotOutputPath.IsEmpty())
	{
		UE_LOG(LogTemp, Warning, TEXT("Trying to generate snapshot, but SnapshotOutputPath is empty!"));
		return;
	}

	if (!m_pComponentRegistry)
	{
		UE_LOG(LogTemp, Warning, TEXT("Trying to generate snapshot, but component registry is null!"));
		return;
	}

	UE_LOG(LogTemp, Display, TEXT("** Begin saving snapshot at: %s **"), *SnapshotOutputPath);

	worker::List<worker::Entity> entities;

	for (TActorIterator<AActor> iterator(GWorld->GetWorld()); iterator; ++iterator)
	{
		AActor *Actor = *iterator;
		worker::Entity entity;
		if (ConvertActorToEntity(Actor, entity, true))
			entities.emplace_back(entity);
	}

	generate_snapshot(TCHAR_TO_UTF8(*SnapshotOutputPath), entities);
	UE_LOG(LogTemp, Display, TEXT("** Snapshot generated succesfully **"));
}

void USpatialWorker::ConnectToDeployment(const ESpatialWorkerConnectionType ConnectionType, const bool UseExternalIp, const FString IpAddress, const int Port, const int ConnectionTimeout, const int HeartbeatTimeout, const FString ProjectName, const FString DeploymentName, const FString PlayerToken)
{
	connect((worker::NetworkConnectionType)ConnectionType, UseExternalIp, TCHAR_TO_UTF8(*IpAddress), Port, ConnectionTimeout, HeartbeatTimeout, TCHAR_TO_UTF8(*ProjectName), TCHAR_TO_UTF8(*DeploymentName), TCHAR_TO_UTF8(*PlayerToken));
}

FString USpatialWorker::CreatePlayerIdentityToken(const FString LoginToken, const FString PlayerId, const FString DisplayName)
{
	return UTF8_TO_TCHAR(create_player_identity_token(TCHAR_TO_UTF8(*LoginToken), TCHAR_TO_UTF8(*PlayerId), TCHAR_TO_UTF8(*DisplayName)).c_str());
}

void USpatialWorker::Connect()
{
	/* Override connection variables by engine input variables if provided */
	FString connection_type;
	ESpatialWorkerConnectionType ConnectionType = ESpatialWorkerConnectionType::eSWCT_RAKNET;
	FParse::Value(FCommandLine::Get(), TEXT("spatialos_connection_type"), connection_type);
	if (!connection_type.IsEmpty())
	{
		if (connection_type.Compare("RAKNET") == 0)
			ConnectionType = ESpatialWorkerConnectionType::eSWCT_RAKNET;
		else if (connection_type.Compare("TCP") == 0)
			ConnectionType = ESpatialWorkerConnectionType::eSWCT_TCP;
		else if (connection_type.Compare("KCP") == 0)
			ConnectionType = ESpatialWorkerConnectionType::eSWCT_KCP;
	}
	//
	FString external_ip;
	bool UseExternalIp = false;
	FParse::Value(FCommandLine::Get(), TEXT("spatialos_external_ip"), external_ip);
	if (!external_ip.IsEmpty())
	{
		if (external_ip.Compare("TRUE") == 0)
			UseExternalIp = true;
		else if (external_ip.Compare("FALSE") == 0)
			UseExternalIp = false;
	}
	//
	FString IpAddress = "127.0.0.1";
	FParse::Value(FCommandLine::Get(), TEXT("spatialos_address_ip"), IpAddress);
	//
	FString address_port;
	int Port = 7777;
	FParse::Value(FCommandLine::Get(), TEXT("spatialos_address_port"), address_port);
	if (!address_port.IsEmpty())
	{
		Port = atoi(std::string(TCHAR_TO_UTF8(*address_port)).c_str());
	}
	//
	FString timeout;
	int ConnectionTimeout = 10000;
	FParse::Value(FCommandLine::Get(), TEXT("spatialos_timeout"), timeout);
	if (!timeout.IsEmpty())
	{
		ConnectionTimeout = atoi(std::string(TCHAR_TO_UTF8(*timeout)).c_str());
	}
	//
	FString heartbeat_timeout;
	int HeartbeatTimeout = 60000;
	FParse::Value(FCommandLine::Get(), TEXT("spatialos_heartbeat_timeout"), heartbeat_timeout);
	if (!heartbeat_timeout.IsEmpty())
	{
		HeartbeatTimeout = atoi(std::string(TCHAR_TO_UTF8(*heartbeat_timeout)).c_str());
	}
	//
	FString ProjectName;
	FParse::Value(FCommandLine::Get(), TEXT("spatialos_project_name"), ProjectName);
	//
	FString DeploymentName;
	FParse::Value(FCommandLine::Get(), TEXT("spatialos_deployment_name"), DeploymentName);
	//
	FString PlayerToken;
	FParse::Value(FCommandLine::Get(), TEXT("spatialos_player_token"), PlayerToken);
	if (!PlayerToken.IsEmpty())
	{
		if (ProjectName.IsEmpty())
			UE_LOG(LogTemp, Warning, TEXT("Worker has player token provided, but ProjectName is empty!"));
		if (DeploymentName.IsEmpty())
			UE_LOG(LogTemp, Warning, TEXT("Worker has player token provided, but DeploymentName is empty!"));
	}
	
	ConnectToDeployment(ConnectionType, UseExternalIp, IpAddress, Port, ConnectionTimeout, HeartbeatTimeout, ProjectName, DeploymentName, PlayerToken);
}

void USpatialWorker::ReserveEntityIds(int NumberOfIds, const FOnEntityIdsReservedDelegate & CallbackFunction, int Timeout)
{
	m_entityIdReservations.emplace(reserve_entity_ids(NumberOfIds, Timeout), CallbackFunction);
}

void USpatialWorker::QueryEntities(int32 ComponentId, FOnEntityQueriedDelegate CallbackFunction, int Timeout)
{
	m_entityQueries.emplace(query_entity(worker::ComponentId(ComponentId), Timeout), CallbackFunction);
}

void USpatialWorker::CreateEntity(FEntitySpawnInfo SpawnInfo)
{
	if (!IsValid(SpawnInfo.ActorClass))
	{
		UE_LOG(LogTemp, Warning, TEXT("Attempted to spawn SpatialOS entity, but ActorClass was not defined. ActorClass must be a valid blueprint entity class."));
		return;
	}

	worker::Entity entity;
	improbable::EntityAclData acl;
	acl = acl.set_read_acl(SpawnInfo.ReadAccess[0].ToSpatial());
	entity.Add<improbable::EntityAcl>(acl);

	SpawnInfo.ApplyToEntity(entity);

	entity.Add<improbable::Metadata>(improbable::MetadataData(TCHAR_TO_UTF8(*SpawnInfo.ActorClass->GetName().LeftChop(2))));

	//entity.Add<improbable::EntityAcl>();

	create_entity(SpawnInfo.EntityId.ToSpatial(), entity, SpawnInfo.Timeout);
}

//void USpatialWorker::SpawnEntity(FEntityCreationInfo CreationInfo, FSpatialEntityId ReservedEntityId)
//{
//	if (CreationInfo.Datas.Num() < 1)
//	{
//		UE_LOG(LogTemp, Warning, TEXT("Cannot spawn entity, because there was no components datas provided."));
//		return;
//	}
//	if (CreationInfo.BlueprintName.IsEmpty())
//	{
//		UE_LOG(LogTemp, Warning, TEXT("Cannot spawn entity, because entity blueprint name is empty."));
//		return;
//	}
//	if (ReservedEntityId.ToSpatial() <= 0)
//	{
//		UE_LOG(LogTemp, Warning, TEXT("Cannot spawn entity, because provided reserved entity id is not valid."));
//		return;
//	}
//
//	/* Initialize entity from creation info. */
//	InitializeEntityFromCreationInfo(CreationInfo);
//
//	/* If there is pre spawned actor assigned to this creation info, we will register its components and register it within the database. */
//	InitializePreSpawnedActor(CreationInfo, ReservedEntityId);
//
//	///* If we are in multiplayer mode, simply send spawn request. */
//	//if (IsConnected())
//	//	SpawnSpatialEntity(CreationInfo.Entity, ReservedEntityId.ToSpatial(), 5000);
//	///* If we are in singleplayer, but there was no pre spawned entity, we will simply call the same code as if entity creation came from network. */
//	//else
//	//	OnNewEntitySpawned(CreationInfo.BlueprintName, ReservedEntityId, CreationInfo);
//
//	/* Cleanup entity creation info. */
//	CreationInfo.DestroyCreationInfo();
//}

void USpatialWorker::RemoveEntity(FSpatialEntityId EntityId)
{
	remove_entity(EntityId.ToSpatial());
}

void USpatialWorker::DeleteSpatialActors()
{
	if (GIsEditor)
		return;

	for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		AActor *Actor = *ActorItr;

		/* Take spatial position component and check if entity should be removed. */
		bool ClearActor = true;
		//if (UActorComponent *ActorPosition = Actor->GetComponentByClass(UPositionComponent::StaticClass()))
		//{
		//	if (UPositionComponent *PositionComponent = Cast<UPositionComponent>(ActorPosition))
		//	{
		//		Actor->Destroy();
		//	}
		//}
	}
}

bool USpatialWorker::IsConnected()
{
	return is_connected();
}

bool USpatialWorker::IsOffline()
{
	return is_offline();
}

FString USpatialWorker::GetWorkerId()
{
	return UTF8_TO_TCHAR(get_worker_id().c_str());
}

FString USpatialWorker::GetWorkerType()
{
	return WorkerTypeName;
}

FWorkerAttributeSet USpatialWorker::GetWorkerLayer()
{
	FWorkerAttributeSet atts;
	atts.Attribute = UTF8_TO_TCHAR(get_worker_layer().c_str());
	return atts;
}

FWorkerRequirementSet USpatialWorker::GetWorkerRequirementSet()
{
	FWorkerRequirementSet req;
	req.Update(get_worker_requirement_set());
	return req;
}

bool USpatialWorker::IsInEditor()
{
	return GIsEditor;
}

std::string USpatialWorker::get_worker_type()
{
	return TCHAR_TO_UTF8(*WorkerTypeName);
}

std::string USpatialWorker::get_worker_layer()
{
	return TCHAR_TO_UTF8(*WorkerLayerName);
}

bool USpatialWorker::ConvertActorToEntity(AActor * pActor, worker::Entity & entity, const bool ExcludeNonPersistent)
{
	if (!pActor)
	{
		UE_LOG(LogTemp, Warning, TEXT("Trying to create spatial entity out of null Actor!"));
		return false;
	}

	/* Check if entity is actually spatial entity by finding USpatialEntityComponent. */
	UActorComponent *pSpatialActorEntityComponent = pActor->GetComponentByClass(USpatialEntityComponent::StaticClass());

	if (!IsValid(pSpatialActorEntityComponent))
		return false;

	/* Cast to actual entity component. */
	USpatialEntityComponent *pEntityComponent = Cast <USpatialEntityComponent>(pSpatialActorEntityComponent);

	/* If entity is not persistent, then it shouldn't be saved into snapshot. */
	if (ExcludeNonPersistent)
	{
		if (!pEntityComponent->IsPersistent())
			return false;
	}

	/* Write access requirements. */
	worker::Map<std::uint32_t, improbable::WorkerRequirementSet> WriteAccess;

	/* Get all spatial components in this entity. */
	TArray<USpatialBaseComponent*> components;
	pActor->GetComponents<USpatialBaseComponent>(components);

	/* Save read access requirements. */
	const improbable::WorkerRequirementSet ReadAccess = pEntityComponent->GetReadAccessRequirements().ToSpatial();
	/* Save authority granting requirements. */
	const improbable::WorkerRequirementSet AuthorityGranting = pEntityComponent->GetAuthorityGrantingWorkerRequirements().ToSpatial();

	/* Iterate through all spatial components. */
	for (USpatialBaseComponent *pComponent : components)
	{
		/* Add this component to the entity provided. */
		pComponent->ApplyToEntity(entity);
		/* Add write access to this component. It will be granted to authority granting worker. */
		WriteAccess.emplace(pComponent->GetComponentId(), AuthorityGranting);
	}

	/* There are a few components entity must have in order to be networked. These are Metadata and EntityAcl. Persistence component is added too if entity should persist in snapshots. */

	/* Metadata component will store entity blueprint name. */
	FString EntityClassName = pActor->GetClass()->GetName();
	EntityClassName.RemoveFromEnd("_C");
	entity.Add<improbable::Metadata>(improbable::MetadataData(std::string(TCHAR_TO_UTF8(*EntityClassName))));
	WriteAccess.emplace(improbable::Metadata::ComponentId, AuthorityGranting);

	/* Persistence allows server to save entity into snapshots if needed. */
	if (pEntityComponent->IsPersistent())
	{
		entity.Add<improbable::Persistence>(improbable::PersistenceData());
		WriteAccess.emplace(improbable::Persistence::ComponentId, AuthorityGranting);
	}

	/* EntityAcl component manages all component authorities. */
	entity.Add<improbable::EntityAcl>(improbable::EntityAclData(ReadAccess, WriteAccess));
	WriteAccess.emplace(improbable::EntityAcl::ComponentId, AuthorityGranting);
	return true;
}

AActor * USpatialWorker::GetActorFromDatabase(FSpatialEntityId EntityId)
{
	return database_get_entity(EntityId.ToSpatial());
}

worker::List<worker::Entity> USpatialWorker::get_offline_entities(worker::EntityId begin_id, const bool initialize_components)
{
	/* List of entities returned in the end. */
	worker::List<worker::Entity> entities;

	/* Iterate through all actors in the scene. */
	for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		AActor *pActor = *ActorItr;

		/* Check if this actor is actually spatial actor by checking if it contains spatial entity component. */
		UActorComponent *pActorSpatialComponent = pActor->GetComponentByClass(USpatialEntityComponent::StaticClass());

		if (!pActorSpatialComponent)
			continue;

		USpatialEntityComponent *pSpatialEntityComponent = Cast<USpatialEntityComponent>(pActorSpatialComponent);

		/* If this actor is not offline enabled, then ignore it. */
		if (!pSpatialEntityComponent->IsOfflineEnabled())
			continue;

		/* Entity to be built by this actor. */
		worker::Entity entity;

		/* Now properly iterate through all spatial components based on USpatialBaseComponent. */
		TArray<USpatialBaseComponent*> components;
		pActor->GetComponents<USpatialBaseComponent>(components);

		for (USpatialBaseComponent *pComponent : components)
		{			
			if(initialize_components) pComponent->RegisterSpatialComponentFromInternalSnapshot(begin_id, pComponent->GetOfflineAuthority());
			pComponent->ApplyToEntity(entity);
		}

		entities.emplace_back(entity);

		/* Iterate begin_id. */
		begin_id++;
	}

	return entities;
}

std::string USpatialWorker::get_unique_guid(const int strength)
{
	const FString guid = FGuid::NewGuid().ToString();
	return TCHAR_TO_UTF8(*guid);
}

void USpatialWorker::on_connection_succeeded()
{
	USpatialEntityComponent::RegisterDispatcher();
	OnRegisterComponentsDispatchers();
	OnConnectionSucceededEvent();
}

void USpatialWorker::on_connection_failed()
{
	OnConnectionFailedEvent();
}

void USpatialWorker::on_entity_ids_reserved(const worker::List<worker::EntityId>& reservedIds, const worker::RequestId<worker::ReserveEntityIdsRequest> & reservation)
{
	auto it = m_entityIdReservations.find(reservation);
	if (it != m_entityIdReservations.end())
	{
		TArray<FSpatialEntityId> reservedIdsUnreal;
		for (worker::EntityId id : reservedIds)
		{
			reservedIdsUnreal.Add(FSpatialEntityId(id));
		}

		it->second.ExecuteIfBound(reservedIdsUnreal);
		m_entityIdReservations.erase(it);
	}	
}

void USpatialWorker::on_entity_queried(const worker::Map<worker::EntityId, worker::Entity>& queriedEntities, const worker::RequestId<worker::EntityQueryRequest> & query)
{
	auto it = m_entityQueries.find(query);
	if (it != m_entityQueries.end())
	{
		TArray<FSpatialEntityId> queriedIds;
		for (auto _it : queriedEntities)
		{
			queriedIds.Add(FSpatialEntityId(_it.first));
		}

		it->second.ExecuteIfBound(queriedIds);
		m_entityQueries.erase(it);
	}
}

AActor * USpatialWorker::on_create_entity(const worker::EntityId & entityId, const std::string & entityName)
{
	AActor *pActor = OnCreateEntityEvent(FSpatialEntityId(entityId), UTF8_TO_TCHAR(entityName.c_str()));

	/* If provided actor is valid, iterate through its spatial components and initialize them. */
	if (IsValid(pActor))
	{
		/* Get entity associated with this actor within the view. */
		worker::Entity entity;
		if (!IsOffline())
		{
			auto entity_it = m_pView->Entities.find(entityId);
			if (entity_it != m_pView->Entities.end())
				entity = entity_it->second;
		}
		else
		{
			auto entity_it = m_offlineEntityDatabase.find(entityId);
			if (entity_it != m_offlineEntityDatabase.end())
				entity = entity_it->second;
		}

		worker::Map<worker::ComponentId, worker::Authority> auth;
		auto authority_it = m_pView->ComponentAuthority.find(entityId);		
		if (authority_it != m_pView->ComponentAuthority.end())
		{
			auth = authority_it->second;
		}

		/* TODO: Check - maybe order of the components inside the entity is always equal to those in blueprint which will make it faster to find authority for component by index. */

		TArray<UActorComponent *> components = pActor->GetComponentsByClass(USpatialBaseComponent::StaticClass());
		for (UActorComponent *pActorComponent : components)
		{
			USpatialBaseComponent *pComponent = Cast<USpatialBaseComponent>(pActorComponent);

			/* Get current authority for this component. */
			worker::Authority authority = worker::Authority::kNotAuthoritative;
			auto it = auth.find(pComponent->GetComponentId());
			if (it != auth.end())
			{
				authority = it->second;
			}

			/* Essential part, registers component within event updates dispatcher system. Component is ready to interact with the world. */
			pComponent->RegisterSpatialComponent(entityId, m_bIsOffline ? pComponent->GetOfflineAuthority() : authority, entity);
		}
	}

	return pActor;
}
//
//void USpatialWorker::InitializePreSpawnedActor(FEntityCreationInfo CreationInfo, FSpatialEntityId EntityId, const bool AddToDatabase)
//{
//	/* Check if creation info has pre spawned actor assigned and if entity id provided is valid. */
//	//if (!IsValid(CreationInfo.PreSpawnedActor) || EntityId.ToSpatial() <= 0)
//	//	return;
//
//	//TArray<USpatialEntityComponent*> SpatialComponents;
//	//CreationInfo.PreSpawnedActor->GetComponents<USpatialEntityComponent>(SpatialComponents);
//
//	//for (USpatialEntityComponent *SpatialComponent : SpatialComponents)
//	//{
//	//	for (USpatialComponentDataCreation *Data : CreationInfo.Datas)
//	//	{
//	//		if (Data->GetAssociatedComponentId() == SpatialComponent->GetComponentId())
//	//		{
//	//			/* Depending on current mode use the right function. */
//	//			if (IsSingleplayer())
//	//				SpatialComponent->RegisterSpatialComponentSingleplayer(EntityId.ToSpatial(), Data, CreationInfo.PreSpawnedActorEnableOnAddEvent);
//	//			else 
//	//				SpatialComponent->RegisterSpatialComponentWithData(EntityId.ToSpatial(), Data, CreationInfo.PreSpawnedActorEnableOnAddEvent);
//
//	//			/* Break datas loop here so it doesn't look any further. */
//	//			break;
//	//		}
//	//	}
//	//}
//
//	///* If adding to database is required, add it. */
//	//if (AddToDatabase)
//	//	Database->AddEntity(CreationInfo.PreSpawnedActor, EntityId.ToSpatial());
//}

void USpatialWorker::on_worker_registered()
{
	OnWorkerRegistered();
}

void USpatialWorker::on_became_active()
{
	OnBecomeActiveEvent();
}

//
//void USpatialWorker::InitializeEntityFromCreationInfo(FEntityCreationInfo & CreationInfo)
//{
//	//worker::Map<std::uint32_t, improbable::WorkerRequirementSet> WriteAcl;
//	//for (int i = 0; i < CreationInfo.Datas.Num(); i++)
//	//{
//	//	USpatialComponentDataCreation *Data = CreationInfo.Datas[i];
//	//	FWorkerRequirementSet Requirements = Data->WriteAccess;
//
//	//	Data->ApplyDataToEntity(CreationInfo.Entity);
//	//	WriteAcl.emplace(Data->GetAssociatedComponentId(), Requirements.ToSpatial());
//	//}
//	//improbable::WorkerRequirementSet ReadAccess;
//	//for (int i = 0; i < CreationInfo.ReadAccess.Num(); i++)
//	//{
//	//	FWorkerRequirementSet Requirements = CreationInfo.ReadAccess[i];
//	//	for (int j = 0; j < Requirements.AttributeSet.Num(); j++)
//	//	{
//	//		auto att = ReadAccess.attribute_set();
//	//		att.emplace_back(Requirements.AttributeSet[j].ToSpatial());
//	//		ReadAccess.set_attribute_set(att);
//	//	}
//	//}
//	//improbable::WorkerRequirementSet AbsoluteAuthority = CreationInfo.AuthorityWriteAccess.ToSpatial();
//	///* Add basic spatial components that are required for the entity to exist in the world */
//	//CreationInfo.Entity.Add<improbable::Persistence>(improbable::PersistenceData());
//	//WriteAcl.emplace(improbable::Persistence::ComponentId, AbsoluteAuthority);
//	////
//	//CreationInfo.Entity.Add<improbable::Metadata>(improbable::MetadataData(std::string(TCHAR_TO_UTF8(*CreationInfo.BlueprintName))));
//	//WriteAcl.emplace(improbable::Metadata::ComponentId, AbsoluteAuthority);
//	//WriteAcl.emplace(improbable::EntityAcl::ComponentId, AbsoluteAuthority);
//	//CreationInfo.Entity.Add<improbable::EntityAcl>(improbable::EntityAclData(ReadAccess, WriteAcl));
//}

IComponentRegistry * USpatialWorker::GetComponentRegistry()
{
	return m_pComponentRegistry;
}