#include "Public/Utilities.h"

FSpatialEntityId::FSpatialEntityId()
{
	m_entityId = 0;
}

FSpatialEntityId::FSpatialEntityId(const FSpatialEntityId & entityId)
{
	m_entityId = entityId.m_entityId;
}

FSpatialEntityId::FSpatialEntityId(const worker::EntityId & entityId)
{
	m_entityId = entityId;
}

FSpatialEntityId & FSpatialEntityId::operator=(const FSpatialEntityId & other)
{
	m_entityId = other.m_entityId;
	return *this;
}

bool FSpatialEntityId::operator==(const FSpatialEntityId & other) const
{
	return m_entityId == other.m_entityId;
}

bool FSpatialEntityId::operator==(const worker::EntityId & other) const
{
	return m_entityId == other;
}

bool FSpatialEntityId::operator!=(const FSpatialEntityId & other) const
{
	return m_entityId != other.m_entityId;
}

worker::EntityId FSpatialEntityId::ToSpatial()
{
	return m_entityId;
}

uint32 GetTypeHash(FSpatialEntityId const & Rhs)
{
	int64 x = Rhs.m_entityId;
	x = ((x >> 16) ^ x) * 0x45d9f3b;
	x = ((x >> 16) ^ x) * 0x45d9f3b;
	x = (x >> 16) ^ x;
	return static_cast<uint32>(x);
}

improbable::WorkerAttributeSet FWorkerAttributeSet::ToSpatial()
{
	return improbable::WorkerAttributeSet{ worker::List<std::string>{ {TCHAR_TO_UTF8(*Attribute)}} };
}
void FWorkerAttributeSet::Update(improbable::WorkerAttributeSet Value)
{
	if (Value.attribute().empty())
		Attribute.Empty();
	else
		Attribute = FString(Value.attribute()[0].c_str());
}

improbable::WorkerRequirementSet FWorkerRequirementSet::ToSpatial()
{
	improbable::WorkerRequirementSet Value;
	worker::List<improbable::WorkerAttributeSet> Atts;
	for (FWorkerAttributeSet att : AttributeSet)
	{
		Atts.emplace_back(att.ToSpatial());
	}
	Value = Value.set_attribute_set(Atts);
	return Value;
}
void FWorkerRequirementSet::Update(improbable::WorkerRequirementSet Value)
{
	if (Value.attribute_set().empty())
		AttributeSet.Empty();
	else
	{
		for (improbable::WorkerAttributeSet att : Value.attribute_set())
		{
			FWorkerAttributeSet Att;
			Att.Update(att);
			AttributeSet.Add(Att);
		}
	}
}

void FEntitySpawnInfo::DestroySpawnInfo()
{
	for (USpatialComponentDataForge *pForge : Forges)
	{
		pForge->ConditionalBeginDestroy();
	}
	Forges.Empty();
}

void FEntitySpawnInfo::ApplyToEntity(worker::Entity & entity)
{
	for (USpatialComponentDataForge *pForge : Forges)
	{
		pForge->ApplyToEntity(entity);
	}
}
