#include "SpatialEntityComponent.h"
#include "SpatialController.h"

void USpatialEntityComponentDataForge::ApplyToEntity(worker::Entity & entity)
{
	improbable::PositionData data;
	data = data.set_coords(improbable::Coordinates(Coords.X * 0.1f, Coords.Z * 0.1f, Coords.Y * 0.1f));
	entity.Add<improbable::Position>(data);
	//
	worker::Map<uint32_t, improbable::WorkerRequirementSet> writeAcl = entity.Get<improbable::EntityAcl>()->component_write_acl();
	auto it = writeAcl.find(improbable::Position::ComponentId);
	if (it != writeAcl.end())
	{
		it->second = WriteAccess.ToSpatial();
	}
	else
	{
		writeAcl.emplace(improbable::Position::ComponentId, WriteAccess.ToSpatial());
	}
	entity.Get<improbable::EntityAcl>()->set_component_write_acl(writeAcl);
}

USpatialComponentDataForge *USpatialEntityComponentDataForge::Create(FWorkerRequirementSet write_access, FVector coords)
{
	USpatialEntityComponentDataForge *pData = NewObject<USpatialEntityComponentDataForge>();
	pData->WriteAccess = write_access;
	//
	pData->Coords = coords;
	//
	return pData;
}

void USpatialEntityComponent::RegisterDispatcher()
{
	register_dispatcher();
}

void USpatialEntityComponent::RegisterSpatialComponent(const worker::EntityId initial_id, const worker::Authority initial_authority, const worker::Entity & input_entity)
{
	register_component(initial_id, initial_authority, input_entity);
}

void USpatialEntityComponent::RegisterSpatialComponentFromInternalSnapshot(const worker::EntityId initial_id, const worker::Authority initial_authority)
{
	register_component(initial_id, initial_authority, snapshot_component());
}

worker::ComponentId USpatialEntityComponent::GetComponentId()
{
	return SpatialComponentType::ComponentId;
}

improbable::PositionData USpatialEntityComponent::snapshot_component()
{
	/* PER VARIABLE */
	FVector Coords;
	OnSnapshotComponentEvent(Coords);

	improbable::PositionData data;
	data = data.set_coords(improbable::Coordinates(Coords.X * 0.1f, Coords.Z * 0.1f, Coords.Y * 0.1f));
	return data;
}

void USpatialEntityComponent::OnSnapshotComponentEvent_Implementation(FVector & Coords)
{
	Coords = GetOwner()->GetActorLocation();
}

worker::Authority USpatialEntityComponent::GetOfflineAuthority()
{
	return get_offline_authority();
}

void USpatialEntityComponent::ApplyToEntity(worker::Entity & entity)
{
	apply_to_entity(entity);
}

worker::Authority USpatialEntityComponent::get_offline_authority()
{
	return (worker::Authority)OfflineAuthority;
}

bool USpatialEntityComponent::are_backward_updates_enabled()
{
	return EnableBackwardUpdates;
}

bool USpatialEntityComponent::is_sending_updates_blocked()
{
	return BlockSendingUpdates;
}

bool USpatialEntityComponent::is_receiving_updates_blocked()
{
	return BlockReceivingUpdates;
}

USpatialEntityComponent::USpatialEntityComponent()
{
	PrimaryComponentTick.bStartWithTickEnabled = true;
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.TickInterval = InspectorPositionUpdateRate;
}

void USpatialEntityComponent::on_authority_changed()
{
	PrimaryComponentTick.bCanEverTick = m_authority == worker::Authority::kAuthoritative;
}

void USpatialEntityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	const FVector send_position = GetOwner()->GetActorLocation();
	improbable::Position::Data new_data;
	new_data = new_data.set_coords(improbable::Coordinates(send_position.X * 0.1f, send_position.Z * 0.1f, send_position.Y * 0.1f));
	send_update(new_data);
}

bool USpatialEntityComponent::IsOfflineEnabled()
{
	return OfflineEnabled;
}

bool USpatialEntityComponent::IsPersistent()
{
	return Persistent;
}

FWorkerRequirementSet USpatialEntityComponent::GetReadAccessRequirements()
{
	FWorkerRequirementSet RequirementSet;

	USpatialController *pController = USpatialController::GetActiveSpatialController();

	for (TSubclassOf<USpatialWorker> WorkerClass : ReadAccessWorkers)
	{
		USpatialWorker *pWorker = pController->GetWorkerByClass(WorkerClass);
		const FString WorkerLayer = pWorker->GetWorkerLayer().Attribute;
		FWorkerAttributeSet AttributeSet;
		AttributeSet.Attribute = WorkerLayer;
		RequirementSet.AttributeSet.Add(AttributeSet);
	}

	return RequirementSet;
}

FWorkerRequirementSet USpatialEntityComponent::GetAuthorityGrantingWorkerRequirements()
{
	FWorkerRequirementSet RequirementSet;

	if (IsValid(AuthorityGrantingWorker))
	{
		USpatialController *pController = USpatialController::GetActiveSpatialController();

		USpatialWorker *pWorker = pController->GetWorkerByClass(AuthorityGrantingWorker);
		const FString WorkerLayer = pWorker->GetWorkerLayer().Attribute;
		FWorkerAttributeSet AttributeSet;
		AttributeSet.Attribute = WorkerLayer;
		RequirementSet.AttributeSet.Add(AttributeSet);
	}

	return RequirementSet;
}

improbable::PositionData USpatialEntityComponentDataForge::Get()
{
	improbable::PositionData data;
	//
	data = data.set_coords(improbable::Coordinates(Coords.X * 0.1f, Coords.Z * 0.1f, Coords.Y * 0.1f));
	//
	return data;
}
