#pragma once
#include "improbable/worker.h"
#include "improbable/standard_library.h"

#define SPATIALOS_COMPONENTS_REGULAR(name) name,
#define REGISTER_COMPONENTS(registry, spatial) \
struct registry \
{ \
	registry() : component_registry() {} \
	worker::Components<spatial(SPATIALOS_COMPONENTS_REGULAR) improbable::Position, improbable::Metadata, improbable::Persistence, improbable::EntityAcl> component_registry; \
};

/* This structure allows for component registration. Create structure that derives from that interface in order to register custom components. */
struct IComponentRegistry
{
	virtual ~IComponentRegistry() {}
	virtual worker::ComponentRegistry &GetComponentRegistry() = 0;
};

/* Example implementation */
/*
#define CUSTOM_COMPONENTS(cmpt) \
	cmpt(namespace::Component) \
	cmpt(namespace::AnotherComponent)

REGISTER_COMPONENTS(SComponentRegistryStructure, CUSTOM_COMPONENTS)

struct SComponentRegistry : public IComponentRegistry
{
	virtual worker::ComponentRegistry &GetComponentRegistry() override
	{
		return component_registry.component_registry;
	}
	SComponentRegistryStructure GetComponentRegistryNative() { return component_registry; }
	
private:
	SComponentRegistryStructure component_registry;
};
*/