#pragma once
#include "Controller.h"

/* Template entity component structure that gathers all component related data and functions. It must be derived from by proper entity component that is user-defined. */
template<typename T>
struct IComponent
{
	typedef typename T::Data SpatialDataType;
	typedef T SpatialComponentType;

	/* Destructor makes sure that if dispatcher is destroyed, all callbacks are cleared out. */
	virtual ~IComponent()
	{
		if (m_pDispatcher == this)
		{
			/* TODO: dispatcher destruction should probably destroy all living components too, as they no longer have option to update. */
			m_onAddComponents.clear();
			m_onUpdateComponents.clear();
			m_onAuthorityChangeComponents.clear();

			for (uint64_t callback : m_callbacks)
			{
				GetActiveWorker()->remove_callback(callback);
			}
			delete m_pDispatcher;
		}
		else
		{
			m_onAddComponents.erase(m_entityId);
			m_onUpdateComponents.erase(m_entityId);
			m_onAuthorityChangeComponents.erase(m_entityId);
		}
	}

	/* Should be called only once! It is ignored if offline. User should call this method in order to register component class. Internally it creates dispatcher for this class of component to handle updates for components in the view. */
	static void register_dispatcher()
	{
		if (GetActiveWorker()->is_offline())
			return;

		if (m_pDispatcher)
			return;

		m_pDispatcher = new IComponent<T>();
		m_callbacks.emplace_back(GetActiveWorker()->register_add_component_callback<T>(std::bind(&IComponent<T>::on_add, m_pDispatcher, std::placeholders::_1)));
		m_callbacks.emplace_back(GetActiveWorker()->register_component_update_callback<T>(std::bind(&IComponent<T>::on_update, m_pDispatcher, std::placeholders::_1)));
		m_callbacks.emplace_back(GetActiveWorker()->register_component_authority_change_callback<T>(std::bind(&IComponent<T>::on_authority_change, m_pDispatcher, std::placeholders::_1)));
	}

	/* Sets up component by using passed entity and entity id. Extracts data for this component from the passed entity. If the data is not found, it means component hasn't been added yet, and so it is registered to receive on_added callback, otherwise it calls on_added event right away. It also registers component within its dispatcher so that it can receive the updates and authority callbacks. */
	void register_component(const worker::EntityId initial_id, const worker::Authority initial_authority, const worker::Entity & input_entity)
	{
		m_entityId = initial_id;
		m_authority = initial_authority;

		/* Check validity of this component's data inside input entity. */
		const worker::Option<const T::Data&> data = input_entity.Get<T>();
		
		/* If data is valid, it means component has been previously added to the view, and we can assign initial data, and call on_added event right away. */
		if (!data.empty())
		{
			m_data = *data;
			on_added();

			/* Component that was listened for was added from the networking system. Now we will start listening for its authority and updates. */
			m_onAuthorityChangeComponents.emplace(m_entityId, this);
			m_onUpdateComponents.emplace(m_entityId, this);

			/* If authority is different to non authoritative, we need to call authority changed event, because it might be essential for some game logic. */
			if (m_authority != worker::Authority::kNotAuthoritative)
				on_authority_changed();
		}

		/* If data wasn't found within current view, it means that the component is yet to be added from network, which means that we need to register this component for receiving on_add callback when the time is right. */
		else
			register_component_listener(this);
	}

	/* Sets up component by passed entity id and initial authority, and initial data. */
	void register_component(const worker::EntityId initial_id, const worker::Authority initial_authority, const typename T::Data initial_data)
	{
		m_entityId = initial_id;
		m_authority = initial_authority;
		m_data = initial_data;
		on_added();

		/* Component that was listened for was added from the networking system. Now we will start listening for its authority and updates. */
		m_onAuthorityChangeComponents.emplace(m_entityId, this);
		m_onUpdateComponents.emplace(m_entityId, this);

		/* If authority is different to non authoritative, we need to call authority changed event, because it might be essential for some game logic. */
		if (m_authority != worker::Authority::kNotAuthoritative)
			on_authority_changed();
	}

	static worker::ComponentId get_component_id()
	{
		return T::ComponentId;
	}

	/* Returns component data. */
	typename T::Data get_data()
	{
		return m_data;
	} 

	/* Returns authority for this component. */
	worker::Authority get_authority()
	{
		return m_authority;
	}

	/* Handy method to check if we are authoritative over this component. */
	bool is_authoritative()
	{
		return m_authority == worker::Authority::kAuthoritative;
	}

	/* Sends component update to the network */
	void send_update(typename T::Data new_data)
	{
		/* Check if we are authoritative over the component. TODO: Spatial will not send it anyways, so this check might be over careful acting. */
		if (!is_authoritative())
			return;

		/* Check if sending is not blocked. */
		if (is_sending_updates_blocked())
			return;

		/* Send only updates that don't equal current data. */
		if (m_data == new_data)
			return;

		/* If authoritative, we can set up new component data. */
		m_data = new_data;

		if (!GetActiveWorker())
			return;

		/* If we are offline, we will send update right to on_updated event function to simulate networking. */
		if (GetActiveWorker()->is_offline())
		{
			/* Send update event if backward updates are enabled, and if updates are not blocked. */
			if (are_backward_updates_enabled())
				on_updated();
		}
		/* If online, send proper update. */
		else
		{
			T::Update update;
			update = update.FromInitialData(m_data);
			GetActiveWorker()->send_component_update<T>(m_entityId, update);
		}
	}

	/* Should be overriden by component class to provide specific data for snapshoting. However, if not overriden, it will return current component data. */
	virtual typename T::Data snapshot_component()
	{
		return m_data;
	}

	/* Adds this component to the provided entity reference. */
	void apply_to_entity(worker::Entity & entity)
	{
		entity.Add<T>(snapshot_component());
	}

protected:
	/* Called automatically by the event system if component was added. */
	void on_add(const worker::AddComponentOp<T> & component)
	{
		/* Search this entity id within components. If found, call on added event. If not, do nothing. */
		auto it = m_onAddComponents.find(component.EntityId);
		if (it != m_onAddComponents.end())
		{
			/* Component that was listened for was added from the networking system. Now we will start listening for its authority and updates. */
			m_onAuthorityChangeComponents.emplace(it->first, it->second);
			m_onUpdateComponents.emplace(it->first, it->second);

			/* Actually set up data for this component and call on_added event. */
			it->second->m_data = component.Data;
			it->second->on_added();

			/* Remove this component from this list so it doesn't make iteration bigger. */
			m_onAddComponents.erase(it);
		}
	}
	
	/* Called automatically by the event system when update for component comes. */
	void on_update(const worker::ComponentUpdateOp<T> & component)
	{
		/* Search this entity id within components. If found, call on updated event. If not, do nothing. */
		auto it = m_onUpdateComponents.find(component.EntityId);
		if (it != m_onUpdateComponents.end())
		{
			it->second->m_data = component.Update.ToInitialData();

			/* If receiving updates is blocked, do not call on_updated event. */
			if (!it->second->is_receiving_updates_blocked())
			{
				if (it->second->is_authoritative())
				{
					if(it->second->are_backward_updates_enabled())
						it->second->on_updated();
				}
				else
					it->second->on_updated();
			}
				
		}
	}

	/* Called automatically by the event system when component authority update comes. */
	void on_authority_change(const worker::AuthorityChangeOp & authority)
	{
		/* Search this entity id within components. If found, call on authority changed event. If not, do nothing. */
		auto it = m_onAuthorityChangeComponents.find(authority.EntityId);
		if (it != m_onAuthorityChangeComponents.end())
		{
			it->second->m_authority = authority.Authority;
			it->second->on_authority_changed();
		}
	}

	/* Called automatically by the event system when component was added and its data was set. It should be overriden by the user's component for the functionality. */
	virtual void on_added() {}

	/* Called automatically by the event system when component was updated and its data was set. It should be overriden by the user's component for the functionality. */
	virtual void on_updated() {}

	/* Called automatically by the event system when component authority was updated and set. It should be overriden by the user's component for the functionality. */
	virtual void on_authority_changed() {}

	/* TODO: Check if it's possible to turn worker::Map into worker::List in order to reduce memory usage. Components hold their entity ids anyway, so it might be reasonanble to recognize them through it. */

	/* Registers component for on_add callback. */
	static void register_component_listener(IComponent<T> *new_component)
	{
		if (!new_component)
			return;

		/* Component is added to the list, it will now be listening to when it's network added. */
		m_onAddComponents.emplace(new_component->m_entityId, new_component);
	}

	/* Should be user defined and should return offline authority. It is used when component is registerd in offline mode. */
	virtual worker::Authority get_offline_authority()
	{
		return worker::Authority::kNotAuthoritative;
	}

	/* Should be user defined and should return true if user wants to enable backward updates. */
	virtual bool are_backward_updates_enabled()
	{
		return true;
	}

	/* Should be user defined and should return true if user wants to block sending updates. */
	virtual bool is_sending_updates_blocked()
	{
		return false;
	}

	/* Should be user defined and should return true if user wants to block receiving updates. */
	virtual bool is_receiving_updates_blocked()
	{
		return false;
	}

protected:
	/* Underlying component data that is SpatialOS type. It can be accessed through accessor function. */
	typename T::Data m_data;

	/* Entity id associated with this component's owner entity. */
	worker::EntityId m_entityId = 0;

	/* Underlying component authority ran by SpatialOS updates. */
	worker::Authority m_authority = worker::Authority::kNotAuthoritative;

	/* Dispatcher is the static instance of the class template. It handles updated for all entities that contain this component. */
	static typename IComponent<T> * m_pDispatcher;

	/* Dispatcher stores three types of components lists, depending on callback type. It's optimized in the way that if components are added to one list, and no longer needed, thay can be deleted from the list, so that iterations don't consider them. */
	/* Components list for adding component callbacks. Components on this list will have their on_added event called when they come from networking, but were previously registered to this list. After event is called, they disappear from this list. */
	static worker::Map<worker::EntityId, IComponent<T>*> m_onAddComponents;
	/* Components list for updating component callbacks. Components on this list will have their on_updated event called, everytime update comes(unless it's blocked), they disappear from this list only when they are deleted from the view. */
	static worker::Map<worker::EntityId, IComponent<T>*> m_onUpdateComponents;
	/* Components list for updating authority state of the component. Components on this list will have their on_authority_changed event called, everytime authority changes, they disappear from this list only when they are deleted from the view. */
	static worker::Map<worker::EntityId, IComponent<T>*> m_onAuthorityChangeComponents;

	/* List of callbacks associated with dispatcher. User should not modify it. When dispatcher is destroyed, destructor makes sure callbacks are unregistered from the view, so that internal dispatcher doesn't crash because of using non existing object. */
	static worker::List<uint64_t> m_callbacks;
};

/* Static members need declaration. */

template<typename T>
IComponent<T> * IComponent<T>::m_pDispatcher = nullptr;

template<typename T>
worker::Map<worker::EntityId, IComponent<T>*> IComponent<T>::m_onAddComponents;

template<typename T>
worker::Map<worker::EntityId, IComponent<T>*> IComponent<T>::m_onUpdateComponents;

template<typename T>
worker::Map<worker::EntityId, IComponent<T>*> IComponent<T>::m_onAuthorityChangeComponents;

template<typename T>
worker::List<uint64_t> IComponent<T>::m_callbacks;