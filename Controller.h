#pragma once
#include "Worker.h"

struct IController
{	
	virtual ~IController();

	void RegisterWorkerType(IWorker *pNewWorker);

	void UnregisterWorker(IWorker *pWorker);

	IWorker *FindWorkerByTypeName(std::string workerTypeName);

	void SetActiveWorker(std::string workerTypeName);

	void SetActiveWorker(IWorker *pWorker);

	IWorker *GetActiveWorker();

	static void SetActiveController(IController *pController);

	static IController *GetActiveController();

protected:
	virtual void RegisterWorker_Internal(IWorker *pNewWorker);

	virtual IWorker *FindWorkerByTypeName_Internal(std::string workerTypeName);

	virtual int GetWorkerIndex(std::string workerTypeName);
	virtual int GetWorkerIndex(IWorker *pWorker);

	virtual void OnBecameActive() {}

protected:
	worker::List<IWorker *> m_workers;
	int m_iActiveWorker = -1;
	static IController *m_pActiveController;
};

static IController *GetActiveController()
{
	return IController::GetActiveController();
}

static IWorker *GetActiveWorker()
{
	if (!GetActiveController())
		return nullptr;

	return GetActiveController()->GetActiveWorker();
}