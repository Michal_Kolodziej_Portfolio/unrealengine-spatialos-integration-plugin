#include "Worker.h"
#include "improbable/detail/development_auth.i.h"

bool token_connected_callback(const worker::QueueStatus & qs)
{
	return true;
}

std::string IWorker::create_player_identity_token(const std::string & loginToken, const std::string & playerIdentificator, const std::string & displayName)
{
	if (loginToken.empty())
		return "";

	worker::alpha::PlayerIdentityTokenRequest tokenRequest;
	tokenRequest.DevelopmentAuthenticationToken = loginToken;
	tokenRequest.DisplayName = displayName;
	tokenRequest.PlayerId = playerIdentificator;

	auto future = worker::alpha::CreateDevelopmentPlayerIdentityTokenAsync("locator.improbable.io", 444, tokenRequest);
	auto response = future.Get();
	return response.PlayerIdentityToken;
}

void IWorker::connect(const worker::NetworkConnectionType & connectionType, const bool & useExternalIP, const std::string & ip, const int & port, const int & connectionTimeout, const int & heartbeatTimeout, const std::string & projectName, const std::string & deploymentName, const std::string & loginToken, const std::string & playerIdentityToken)
{
	if (m_bIsOffline)
	{
		connect_offline();
		on_connection_succeeded();
		return;
	}

	/* Create connection parameters. */
	worker::ConnectionParameters params;
	params.Network.ConnectionType = connectionType;

	/* Handle RakNet specific. */
	if (connectionType == worker::NetworkConnectionType::kRaknet)
		params.Network.RakNet.HeartbeatTimeoutMillis = heartbeatTimeout;

	params.Network.UseExternalIp = useExternalIP;
	params.WorkerType = m_sWorkerType;

	/* Check if everything for player token connection is filled out, this would mean we want to use player token for connection. */
	if (!projectName.empty() && !deploymentName.empty() && !loginToken.empty() && !playerIdentityToken.empty())
	{
		worker::LocatorParameters locatorParams;
		locatorParams.PlayerIdentity.LoginToken = loginToken;
		locatorParams.PlayerIdentity.PlayerIdentityToken = playerIdentityToken;
		locatorParams.CredentialsType = worker::LocatorCredentialsType::kPlayerIdentity;
		locatorParams.ProjectName = projectName;
		worker::Locator *pLocator = new worker::Locator("locator.improbable.io", locatorParams);
		worker::Future<worker::Connection> m_pConnectionFuture = pLocator->ConnectAsync(m_pComponentRegistry->GetComponentRegistry(), deploymentName, params, token_connected_callback);

		if (m_pConnectionFuture.Wait(connectionTimeout))
			m_pConnection = new worker::Connection(m_pConnectionFuture.Get());

		if (m_pConnection && m_pConnection->IsConnected())
		{
			on_connection_succeeded();
			process();
		}
		else
			on_connection_failed();

		return;
	}

	/* If thread gets here it means it will connect without using player token. */
	worker::Future<worker::Connection> m_pConnectionFuture = worker::Connection::ConnectAsync(m_pComponentRegistry->GetComponentRegistry(), ip, port, m_sWorkerId, params);

	if (m_pConnectionFuture.Wait(connectionTimeout))
	{
		m_pConnection = new worker::Connection(m_pConnectionFuture.Get());
		if (m_pConnection)
			process();
	}
	if (m_pConnection && m_pConnection->IsConnected())
	{
		on_connection_succeeded();
	}
	else
		on_connection_failed();
}

void IWorker::connect_offline()
{
	/* Reset offline highest entity id to 1. */
	m_offlineHighestId = 1;

	/* Convert all possibly existing game entity types into spatial entities by using user defined function. */
	worker::List<worker::Entity> offlineEntities = get_offline_entities(m_offlineHighestId);

	/* Reset offline highest entity id to 1 again, so we can iterate through existing entity ids. */
	m_offlineHighestId = 1;

	/* Add all these entities to offline entity buffer. */
	for (const worker::Entity & entity : offlineEntities)
	{
		database_offline_add_entity(m_offlineHighestId, entity);
		m_offlineHighestId++;
	}
}
